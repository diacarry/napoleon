﻿CREATE TABLE users (
	documento varchar(20) PRIMARY KEY,
	nombre varchar(70) NOT NULL,
	nickname varchar(30) NOT NULL,
	contrasenia varchar(30) NOT NULL,
	rol varchar(20) NOT NULL
);
CREATE TABLE movies (
	nombre varchar(100) PRIMARY KEY,
	imagen varchar(30) NOT NULL,
	descripcion varchar(30) NOT NULL,
	costoAlquiler varchar(30) NOT NULL,
	cantInventario INT,
	cantDisponible INT
);
CREATE TABLE reserve (
	id_reserve SERIAL PRIMARY KEY,
	fk_users varchar(20) REFERENCES users (documento),
	fk_movies varchar(100) REFERENCES movies (nombre),
	fecha varchar(10)
);