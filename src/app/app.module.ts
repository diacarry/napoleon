import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IndexpageComponent } from './angular/pages/indexpage/indexpage.component';
import { ClientListPageComponent } from './angular/pages/client-list-page/client-list-page.component';
import { ConsultPageComponent } from './angular/pages/consult-page/consult-page.component';
import { CreatePageComponent } from './angular/pages/create-page/create-page.component';
import { MovieListPageComponent } from './angular/pages/movie-list-page/movie-list-page.component';
import { RegisterPageComponent } from './angular/pages/register-page/register-page.component';
import { ReserveListPageComponent } from './angular/pages/reserve-list-page/reserve-list-page.component';
import { ViewsPageComponent } from './angular/pages/views-page/views-page.component';
import { NotfoundComponent } from './angular/pages/notfound/notfound.component';

import { LoginformComponent } from './angular/components/forms/loginform/loginform.component';
import { RegisterformComponent } from './angular/components/forms/registerform/registerform.component';
import { RegisterMovieFormComponent } from './angular/components/forms/register-movie-form/register-movie-form.component';
import { RegisterNewUserFormComponent } from './angular/components/forms/register-new-user-form/register-new-user-form.component';

import { NabvarTitleComponent } from './angular/components/general/nabvar-title/nabvar-title.component';
import { NabvarComponent } from './angular/components/general/nabvar/nabvar.component';
import { NabvarAdminComponent } from './angular/components/general/nabvar-admin/nabvar-admin.component';
//import { environment } from 'src/environments/environment';

import { ReservesTableComponent } from './angular/components/tables/reserves-table/reserves-table.component';
import { MoviesTableComponent } from './angular/components/tables/movies-table/movies-table.component';
import { UsersTableComponent } from './angular/components/tables/users-table/users-table.component';

const rutas: Routes = [
  {path: '', component: IndexpageComponent},
  {path: 'index', component: IndexpageComponent},
  {path: 'list', component: ConsultPageComponent},
  {path: 'views', component: ViewsPageComponent},
  {path: 'register', component: RegisterPageComponent},
  {path: 'create', component: CreatePageComponent},
  {path: 'clients', component: ClientListPageComponent},
  {path: 'movies', component: MovieListPageComponent},
  {path: 'reserve', component: ReserveListPageComponent}
];

//const { Client } = require('pg');
/*const connectionDB = {
  user: environment.dataBase.user,
  host: environment.dataBase.host,
  database: environment.dataBase.database,
  passwors: environment.dataBase.password,
  port: environment.dataBase.port
}
const client = new Client(connectionDB);
client.connect();*/
/*client.query('SELECT * FROM movies').then(respuesta => {
    console.log(respuesta.rows),
    client.end()
  }).catch(err => {
    client.end()
  }
  );*/

@NgModule({
  declarations: [
    AppComponent,
    IndexpageComponent,
    ClientListPageComponent,
    ConsultPageComponent,
    CreatePageComponent,
    MovieListPageComponent,
    RegisterPageComponent,
    ReserveListPageComponent,
    ViewsPageComponent,
    NotfoundComponent,
    LoginformComponent,
    RegisterformComponent,
    RegisterNewUserFormComponent,
    RegisterMovieFormComponent,
    NabvarTitleComponent,
    NabvarAdminComponent,
    NabvarComponent,
    ReservesTableComponent,
    MoviesTableComponent,
    UsersTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(rutas)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}