import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-views-page',
  templateUrl: './views-page.component.html',
  styleUrls: ['./views-page.component.css']
})
export class ViewsPageComponent implements OnInit {

  reservas = [
    {id_reserva:'1',fk_user:'1160292',fk_movie:'AvP',fecha:'10/06/2019'},
    {id_reserva:'2',fk_user:'1010632',fk_movie:'La momia',fecha:'12/06/2019'},
    {id_reserva:'3',fk_user:'1100246',fk_movie:'Up',fecha:'14/06/2019'}
  ];
  constructor() { }

  ngOnInit() {
  }

}
