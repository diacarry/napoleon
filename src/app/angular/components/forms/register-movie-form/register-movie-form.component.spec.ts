import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterMovieFormComponent } from './register-movie-form.component';

describe('RegisterMovieFormComponent', () => {
  let component: RegisterMovieFormComponent;
  let fixture: ComponentFixture<RegisterMovieFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterMovieFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterMovieFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
