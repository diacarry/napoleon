import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterNewUserFormComponent } from './register-new-user-form.component';

describe('RegisterNewUserFormComponent', () => {
  let component: RegisterNewUserFormComponent;
  let fixture: ComponentFixture<RegisterNewUserFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterNewUserFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterNewUserFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
