import { Component, OnInit } from '@angular/core';
import { UserServiceService } from 'src/app/angular/services/user-service.service';
import { UserInterface } from 'src/app/angular/interfaces/user-interface';
//import { AlertsService } from 'src/app/angular/services/alerts.service';

@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.css']
})
export class LoginformComponent implements OnInit {

  notFound = false;
  user: UserInterface;
  /*constructor(private alertservice: AlertsService ) { }*/
  constructor(private UserService: UserServiceService ) { }

  ngOnInit(){}
  MyFirstAlert(){
    //this.alertservice.alertinia('Hola a todos');
    //this.UserService.getInfo();
    /*this.UserService.getUsers().subscribe(
      (dataApi: UserInterface[]) => {
        this
      }
    );*/
  }
  getUsuarios(userId: number){
    this.notFound = false;
    this.user = null;
    this.UserService.getUser(userId).subscribe(
      (userfromapi: UserInterface) => {
        this.user = userfromapi;
      }
    ),(err: any) => {
      console.error(err);
      this.notFound = true;
    };
  }
}