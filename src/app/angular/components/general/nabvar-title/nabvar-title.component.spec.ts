import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NabvarTitleComponent } from './nabvar-title.component';

describe('NabvarTitleComponent', () => {
  let component: NabvarTitleComponent;
  let fixture: ComponentFixture<NabvarTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NabvarTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NabvarTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
