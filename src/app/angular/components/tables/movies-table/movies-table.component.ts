import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movies-table',
  templateUrl: './movies-table.component.html',
  styleUrls: ['./movies-table.component.css']
})
export class MoviesTableComponent implements OnInit {

  peliculas = [
    {nombre:'AvP',imagen:'avp.jpg',costAlquiler:'2500',cantDisponible:'1'},
    {nombre:'Up',imagen:'Up.jpg',costAlquiler:'7500',cantDisponible:'3'},
    {nombre:'Men in Black 3',imagen:'MB3',costAlquiler:'9000',cantDisponible:'8'},
    {nombre:'The simpsons',imagen:'Simps.jpg',costAlquiler:'5000',cantDisponible:'10'},
    {nombre:'Titanic',imagen:'Titan.jpg',costAlquiler:'6500',cantDisponible:'2'},
    {nombre:'Rapido y Furioso',imagen:'F&F.png',costAlquiler:'5000',cantDisponible:'6'}
  ];
  constructor() { }

  ngOnInit() {
  }

}
