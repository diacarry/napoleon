import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reserves-table',
  templateUrl: './reserves-table.component.html',
  styleUrls: ['./reserves-table.component.css']
})
export class ReservesTableComponent implements OnInit {

  reservas = [
    {usuario:'123589',movie:'The Simpsons',fecha:'20/16/2019'},
    {usuario:'123589',movie:'Men in Black 3',fecha:'20/16/2019'},
    {usuario:'101683',movie:'Titanic',fecha:'30/16/2019'}
  ];
  constructor() { }

  ngOnInit() {
  }

}
