import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.css']
})
export class UsersTableComponent implements OnInit {

  clientes = [
    {documento:'00012',nombres:'Diego Carranza',nickname:'diacarry',rol:'Administrador'},
    {documento:'34831',nombres:'Jorge Suarez',nickname:'jsuarez',rol:'Usuario'},
    {documento:'25480',nombres:'Juan Novoa',nickname:'jnovoa',rol:'Usuario'},
    {documento:'95632',nombres:'Andres Camacho',nickname:'acamacho',rol:'Usuario'},
    {documento:'95453',nombres:'Arnold Trujillo',nickname:'Atrujillo',rol:'Usuario'}
  ];
  constructor() { }

  ngOnInit() {
  }

}
