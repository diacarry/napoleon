export interface MoviesInterface {
    nombre: string;
    imagen: string;
    descripcion: string;
    costoAlquiler?: number;
    cantInventrio?: number;
    cantDisponible?: number;
}
