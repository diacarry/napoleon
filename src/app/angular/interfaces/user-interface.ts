export interface UserInterface {
    documento: string;
    nombre: string;
    nickName: string;
    contrasenia: string;
    rol?: string;
}
