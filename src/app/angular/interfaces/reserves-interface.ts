export interface ReservesInterface {
    id_reserve: number;
    fk_users: string;
    fk_movies: string;
    fecha: string;
}
