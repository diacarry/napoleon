import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlertsService {

  constructor() { }
  alertinia(msg: string){
    console.log(msg);
    alert(msg);
  }
}