import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserInterface } from '../interfaces/user-interface';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  address = environment.APiConsult
  constructor(private http: HttpClient) {}
  getUser(idUser: number): Observable<UserInterface>{
    const url = this.address
    return this.http.get<UserInterface>(url);
  }
  getUsers()/*: Observable<UserInterface[]>*/{
    console.log(JSON.stringify(this.http.get/*<UserInterface[]>*/(this.address)));
    alert('XD');
  }
}